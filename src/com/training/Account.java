package com.training;

public class Account {
	private String accName;
	private int accNumber;

	/**
	 * @param accNumber
	 * @param accName
	 */
	public Account(int accNumber, String accName) {
		super();
		this.setAccNumber(accNumber);
		this.setAccName(accName);
	}
	public int getAccNumber() {
		return accNumber;
	}
	public void setAccNumber(int accNumber) {
		this.accNumber = accNumber;
	}
	public String getAccName() {
		return accName;
	}
	public void setAccName(String accName) {
		this.accName = accName;
	}
}
